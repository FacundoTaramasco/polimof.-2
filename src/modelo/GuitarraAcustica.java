package modelo;

/**
 * Created by Spirok on 1/8/2015.
 */
public class GuitarraAcustica extends Guitarra {

    // Implementando metodos de la clase abstracta
    @Override
    public void encordarGuitarra() {
        System.out.println("Guitarra acustica encordada");
    }

    @Override
    public void ajustarPuente() {
        System.out.println("Puente ajustado de guitarra acustica");
    }
}
