package modelo;

/**
 * Created by Spirok on 1/8/2015.
 */
public interface Instrumento {

    void afinarInstrumento();
    void tocarInstrumento();

}
