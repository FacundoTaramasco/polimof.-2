package modelo;

/**
 * Created by Spirok on 1/8/2015.
 */
public abstract class Guitarra implements Instrumento {

    // Implementando metodos de la interface
    @Override
    public void tocarInstrumento() {
        System.out.println("Una guitarra esta sonando...");
    }

    @Override
    public void afinarInstrumento() {
        System.out.println("Guitarra afinada");
    }

    // Metodos abstractos que seran implementados por clases que deriven de Guitarra
    public abstract void encordarGuitarra();
    public abstract void ajustarPuente();

}
