import modelo.Guitarra;
import modelo.GuitarraAcustica;
import modelo.GuitarraElectrica;
import modelo.Instrumento;

/**
 * Created by Spirok on 1/8/2015.
 */
public class Main {

    public static void main(String[] args) {

        Instrumento lazer = new GuitarraElectrica();
        gestionarGuitarras(lazer);
        lazer.afinarInstrumento();
        lazer.tocarInstrumento();

        System.out.println("-------------------------------");

        Instrumento cortAcustica = new GuitarraAcustica();
        gestionarGuitarras(cortAcustica);
        cortAcustica.afinarInstrumento();
        cortAcustica.tocarInstrumento();
    }


    private static void gestionarGuitarras(Instrumento ist) {
        if (!(ist instanceof Guitarra)) return;

        // Realizando un Downcasting
        Guitarra guitar = (Guitarra) ist;
        guitar.ajustarPuente();
        guitar.encordarGuitarra();

    }
}
